package com.devcamp.circlecylinderapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlecylinderapi.models.Circle;
import com.devcamp.circlecylinderapi.models.Cylinder;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RestAPIcontrollers {
    @GetMapping("/circle-area")
    public double CircleArea (@RequestParam(required = true, name = "radius") double requesRadius){
        Circle circle = new Circle(requesRadius);
        return circle.getArea();
    }

    @GetMapping("/cylinder-volume")
    public double CircleVolume (@RequestParam(required = true, name = "radius") double requesRadius,
    @RequestParam(required = true, name = "height") double requesHeight){
        Cylinder cylinder = new Cylinder(requesRadius, requesHeight);
        return cylinder.getVolume();
    }
}
